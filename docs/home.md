# doc Docsify

markdown 콘텐츠, 파일 종속성 설정으로 문서 사이트를 구축.

[docsify.js.org](https://docsify.js.org/#/?id=docsify)

## Quick start

```
docsify cli 설치
- npm i docsify-cli -g

docs 폴더 생성
- docsify init ./docs
- index.html, README.md, .nojekll 생성됨

docs 로컬서버
- docsify serve docs

```

## 기능 요약

index.html 파일 : $docsify에 옵션 값을 넣어서 component처럼 관리.

html,css 코드 추가 가능(UI 커스텀 자유로움)

템플릿 구조화된 페이지는 \_coverpage.md, \_sidebar.md,\_navbar.md 로 사용

```
<script>
    window.$docsify = {
        loadNavbar: true, // navbar
        loadSidebar: true, // sidebar
        coverpage: true, // cover or main 영역
        homepage: 'home.md', //  default = README.md -> 원하는 페이지 설정
    }
</script>

```

## 코드

## 다이아그램(Mermaid)

## 테이블

## 확인해보기

다국어 필요한 브랜딩 사이트나 캠페인성 페이지를 사전 렌더링하여 정적 HTML 페이지를 생성하는 데 쓸 수 있지 않을까 하는 기대도 있습니다.
